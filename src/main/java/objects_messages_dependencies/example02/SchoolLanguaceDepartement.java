package objects_messages_dependencies.example02;

/**
 * Created by grazi on 22.08.16.
 */
public class SchoolLanguaceDepartement {

    private SpanishCourse spanishCourse;
    private FrenchCourse frenchCourse;
    private HindiCourse hindiCourse;

    SchoolLanguaceDepartement () {
        spanishCourse = new SpanishCourse();
        frenchCourse = new FrenchCourse();
        hindiCourse = new HindiCourse();
    }
    public void registStudentAtSpanish (Student aStudent) {
        spanishCourse.registerStudent(aStudent);
    }
    public void registStudentAtFrench (Student aStudent) {
        frenchCourse.registerStudent(aStudent);
    }
    public void registStudentAtHindi (Student aStudent) {
        hindiCourse.registerStudent(aStudent);
    }
    public void printOutRegistredStudents () {
        System.out.println("#######################################");
        System.out.println("School has folowing students registred:");
        spanishCourse.printRegistredStudentsOut();
        frenchCourse.printRegistredStudentsOut();
        hindiCourse.printRegistredStudentsOut();
        System.out.println("#######################################");
    }
}
