package objects_messages_dependencies.example02;

/**
 * Created by grazi on 22.08.16.
 */
public class App {
    public static void main(String[] args) {
        Student freddy = new Student("Freddy");
        Student max = new Student("Max");

        SchoolLanguaceDepartement school = new SchoolLanguaceDepartement();
        school.registStudentAtSpanish(freddy);
        school.registStudentAtHindi(max);
        school.printOutRegistredStudents();

        school = null; // No SchoolLanguaceDepartement Object anymore...

        freddy.printNameOut();
        max.printNameOut();

    }
}
