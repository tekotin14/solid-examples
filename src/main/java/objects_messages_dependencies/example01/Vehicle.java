package objects_messages_dependencies.example01;

/**
 * Created by grazi on 22.08.16.
 */
public class Vehicle {
    private Engine engine;
    public Vehicle () {
        engine = new Engine();
    }
    public void operateVehicle () {
        engine.start();
    }
    public void accelerate () {
        System.out.println("accelerating myself...");
    }
}
