package srp.reporting;

/**
 * Created by grazi on 23.08.16.
 */
public class EmployeeReportFormatter extends ReportFormatter {

    public EmployeeReportFormatter(Object object, FormatType formatType) {
        super(object, formatType);
    }

    public String getFormattedEmployee () {
        return getFormattedValue();
    }
}
