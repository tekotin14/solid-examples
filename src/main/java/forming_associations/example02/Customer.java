package forming_associations.example02;

/**
 * Created by grazi on 22.08.16.
 */
public class Customer {
    private NewsPaperCompany newsPaperCompany;

    public void setNewsPaperCompany(NewsPaperCompany aNewsPaperCompany) {
        newsPaperCompany = aNewsPaperCompany;
    }
    public void cancelSubscription () {
        newsPaperCompany.stopPaperDelivery();
    }
}
