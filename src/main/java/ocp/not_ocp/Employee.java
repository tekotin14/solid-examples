package ocp.not_ocp;

/**
 * Created by grazi on 23.08.16.
 */
public class Employee {

    private int id;
    private String name;
    private String departement;
    private boolean working;

    public Employee(int id, String name, String departement, boolean working) {
        this.id = id;
        this.name = name;
        this.departement = departement;
        this.working = working;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", departement='" + departement + '\'' +
                ", working=" + working +
                '}';
    }
}
