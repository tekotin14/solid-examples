package ocp.not_ocp;

/**
 * Created by grazi on 23.08.16.
 */
public class Doctor extends Employee {
    public Doctor(int id, String name, String departement, boolean working) {
        super(id, name, departement, working);
        System.out.println("Doctor in action....");
    }
}
