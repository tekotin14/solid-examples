package ocp.ocp_valid;

/**
 * Created by grazi on 23.08.16.
 */
public abstract class Employee {

    private int id;
    private String name;
    private String departement;
    private boolean working;

    public Employee(int id, String name, String departement, boolean working) {
        this.id = id;
        this.name = name;
        this.departement = departement;
        this.working = working;
    }

    public abstract void performDuties ();

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", departement='" + departement + '\'' +
                ", working=" + working +
                '}';
    }
}
