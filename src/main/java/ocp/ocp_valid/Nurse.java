package ocp.ocp_valid;

/**
 * Created by grazi on 23.08.16.
 */
public class Nurse extends Employee {
    public Nurse(int id, String name, String departement, boolean working) {
        super(id, name, departement, working);
        System.out.println("Nurse in action...");
    }

    @Override
    public void performDuties() {
        checkVitalSigns();
        drawBlood();
        cleanPatientArea();
    }

    // Nurses
    private void checkVitalSigns() {
        System.out.println("checking vitals");
    }

    private void drawBlood () {
        System.out.println("drawing blood");
    }

    private void cleanPatientArea() {
        System.out.println("cleaning Patient Area");
    }




}
