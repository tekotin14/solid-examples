package ocp.ocp_valid;

/**
 * Created by grazi on 23.08.16.
 */
public class Doctor extends Employee {
    public Doctor(int id, String name, String departement, boolean working) {
        super(id, name, departement, working);
        System.out.println("Doctor in action....");
    }

    @Override
    public void performDuties() {
        prescribeMedicine();
        diagnosePatient();
    }

    // Doctors
    private void prescribeMedicine () {
        System.out.println("Prescribe Medicine");
    }

    private void diagnosePatient () {
        System.out.println("Diagnosing Patient");
    }

}
