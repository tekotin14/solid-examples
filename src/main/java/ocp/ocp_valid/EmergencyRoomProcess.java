package ocp.ocp_valid;

/**
 * Created by grazi on 23.08.16.
 */
public class EmergencyRoomProcess {

    public static void main(String[] args) {

        HospitalManagement erDirector = new HospitalManagement();
        Employee peggy = new Nurse(1, "Peggy", "emergency", true);

        erDirector.callUpon(peggy);

        Employee suzan = new Doctor(2, "suzan", "emergency", true);

        erDirector.callUpon(suzan);

    }

}
